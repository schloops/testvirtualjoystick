package com.cis.appstate;

import com.cis.joystick.Joystick;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.BaseAppState;
import com.jme3.math.Vector2f;


/**
 *
 * @author jo
 */
public class JoystickAppState extends BaseAppState{
    private SimpleApplication game;
    private Joystick joystick;
    
    public JoystickAppState(){
    }
    
    @Override
    protected void initialize(Application app) {
        this.game = (SimpleApplication)app;
        
        joystick = new Joystick(game, 
                new Vector2f(60f, 60f), 
                new Vector2f(150f, 150f), 
                "Textures/Gizmos/Joystick/joystickBackground.png", 
                "Textures/Gizmos/Joystick/joystickHandle.png",
                5, 0.50f, false);
        joystick.initialize();
    }

    @Override
    public void update( float tpf ) {
//        joystick.update();
    }
    
    @Override
    protected void cleanup(Application app) {
        joystick.clean();
    }
    
    public Joystick getJoystick() {
        return joystick;
    }

    @Override
    protected void onEnable() {
        joystick.enable();
    }

    @Override
    protected void onDisable() {
        joystick.disable();
    }
    
}

