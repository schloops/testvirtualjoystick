/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cis.appstate;

import com.cis.SpaceShip;
import com.cis.joystick.Joystick;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.BaseAppState;
import com.jme3.input.TouchInput;
import com.jme3.input.controls.TouchListener;
import com.jme3.input.controls.TouchTrigger;
import com.jme3.input.event.TouchEvent;
import com.jme3.math.Vector2f;

/**
 *
 * @author jo
 */
public class SpaceshipInputAppState extends BaseAppState implements TouchListener {
    private SimpleApplication application;
    private SpaceShip spaceShip;
    private Joystick joystick;
    private Vector2f lookedIntoItJoystickValue;
    private boolean isJoysTickEvent;
    
    @Override
    public void onTouch(String name, TouchEvent event, float tpf) {
        isJoysTickEvent = joystick.lookIntoIt(event);
        lookedIntoItJoystickValue = joystick.getLastVectorValue();
        
        if(!isJoysTickEvent){
            //check for another joystick or buttons or whatever other input you want
        }
    }

    @Override
    protected void initialize(Application app) {
        this.application = (SimpleApplication)app;
        spaceShip = application.getStateManager().getState(SpaceShipAppState.class).getShip();
        joystick = application.getStateManager().getState(JoystickAppState.class).getJoystick();
        lookedIntoItJoystickValue = Vector2f.ZERO;
        isJoysTickEvent = false;
    }

    @Override
    public void update( float tpf ) {
        spaceShip.moveShip(lookedIntoItJoystickValue.mult(2f));
    }
    
    @Override
    protected void cleanup(Application app) {
    }

    @Override
    protected void onEnable() {
            application.getInputManager().addMapping("Main_Touch_All", new TouchTrigger(TouchInput.ALL));
            application.getInputManager().addListener(this, "Main_Touch_All");        
    }

    @Override
    protected void onDisable() {
        application.getInputManager().deleteMapping("Main_Touch_All");   }
    
}
