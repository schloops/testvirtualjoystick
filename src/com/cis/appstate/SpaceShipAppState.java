/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cis.appstate;

import com.cis.SpaceShip;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.BaseAppState;
import com.jme3.math.Vector2f;

/**
 *
 * @author jo
 */
public class SpaceShipAppState extends BaseAppState{
    private SimpleApplication application;
    private SpaceShip spaceShip;
    
    @Override
    protected void initialize(Application app) {
        this.application = (SimpleApplication)app;
        
        spaceShip = new SpaceShip(application, new Vector2f(250, 250));
    }

    public SpaceShip getShip(){
        return spaceShip;
    }    
    
    @Override
    protected void cleanup(Application app) {
        
    }

    @Override
    protected void onEnable() {
    }

    @Override
    protected void onDisable() {
    }
    
}
