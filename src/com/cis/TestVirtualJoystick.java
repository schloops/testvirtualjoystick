/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cis;

import com.cis.appstate.JoystickAppState;
import com.cis.appstate.SpaceShipAppState;
import com.cis.appstate.SpaceshipInputAppState;
import com.jme3.app.SimpleApplication;

/**
 *
 * @author jo
 */
public class TestVirtualJoystick extends SimpleApplication{
    
    public static void main(String[] args) {
        TestVirtualJoystick app = new TestVirtualJoystick();
        app.start();
    }
    
    @Override
    public void simpleInitApp() {
        getStateManager().attach(new SpaceShipAppState());
        getStateManager().attach(new JoystickAppState());
        if("Dalvik".equals(System.getProperty("java.vm.name"))){
            getStateManager().attach(new SpaceshipInputAppState());
        }
    }
    
    
    
    
}
