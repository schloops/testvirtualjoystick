package com.cis;

import com.jme3.app.SimpleApplication;
import com.jme3.math.Vector2f;
import com.jme3.ui.Picture;

/**
 *
 * @author jo
 */
public class SpaceShip{
        private final Vector2f location;
        private final Picture spaceShipImage;
        
        public SpaceShip(SimpleApplication application, Vector2f startingLocation){
            this.location = startingLocation;
            
            spaceShipImage = new Picture("spaceShipPic");
            spaceShipImage.setImage(application.getAssetManager(), "Textures/fighter.png", true);
            spaceShipImage.setWidth(68);
            spaceShipImage.setHeight(68);
            spaceShipImage.setPosition(startingLocation.x, startingLocation.y);
            application.getGuiNode().attachChild(spaceShipImage);
        }
        
        public void moveShip(Vector2f offset){
            location.add(offset);
            spaceShipImage.move(offset.x, offset.y, 0);
        }
    }
